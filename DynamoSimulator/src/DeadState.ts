﻿class DeadState extends Phaser.State {
    kills: number;

    init(kills: number) {
        this.kills = kills;
    }

    create() {
        this.game.stage.backgroundColor = "#000000";

        var object = this.game.add.text(this.game.world.centerX, this.game.world.centerY, "You are dead. Maybe stack some defense up?", { fill: "white", fontSize: 32 });
        object.anchor.setTo(0.5, 0.5);

        object = this.game.add.text(this.game.world.centerX, this.game.world.centerY + 40, "Kill count: " + this.kills, { fill: "white", fontSize: 24 });
        object.anchor.setTo(0.5, 0.5);

        object = this.game.add.text(this.game.world.width, this.game.world.height, "Press space (or touch) to try again", { fill: "white", fontSize: 16 });
        object.anchor.setTo(1, 1);
    }

    update() {
        var isActionDown = isActionButtonDown(this.game);
        if (isActionDown) {
            this.game.state.start("main");
        }
    }
}
