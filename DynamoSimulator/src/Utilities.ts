﻿function normalRandom(game: Phaser.Game, mean: number, sd: number) : number {
    var u1 = game.rnd.frac();
    var u2 = game.rnd.frac();
    var randStandard = (Math.sqrt(-2 * Math.log(u1)) * Math.sin(2 * Math.PI * u2));
    return mean + sd * randStandard;
}

function isActionButtonDown(game: Phaser.Game) {
    return game.input.keyboard.isDown(Phaser.KeyCode.SPACEBAR) ||
        game.input.pointer1.isDown || game.input.mousePointer.isDown;
}
