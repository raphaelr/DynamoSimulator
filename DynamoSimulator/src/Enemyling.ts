﻿class Enemyling {
    state: MainState;
    game: Phaser.Game;
    collisions: CollisionHelper;
    sprite: Phaser.Sprite;
    splatBitmap: Phaser.BitmapData;
    ghostSprite: Phaser.Sprite;
    name: string;
    baseFrame: number;

    onGround = false;
    onGroundExtraTime = 0;
    lastFireTime = 0;
    dead = false;
    respawnTimer = 5000;

    constructor(state: MainState, name: string, baseFrame: number) {
        this.game = state.game;
        this.state = state;
        this.name = name;
        this.baseFrame = baseFrame;
    }

    create(collisions: CollisionHelper, group: Phaser.Group, splatBitmap: Phaser.BitmapData) {
        this.collisions = collisions;

        var x = this.game.rnd.integerInRange(700, 820);
        var y = 0;
        this.sprite = this.game.add.sprite(x, y, "enemylings", 0, group);
        this.sprite["enemyling"] = this;
        this.sprite.scale.setTo(3, 3);
        this.sprite.smoothed = false;
        this.sprite.body.fixedRotation = true;
        this.sprite.anchor.setTo(0.5, 0.5);
        this.sprite.body.setCircle(43, 20, 0);
        this.sprite.animations.add("walk", [this.baseFrame*2 + 0, this.baseFrame*2 + 1], 4, true);
        this.sprite.play("walk");
        collisions.registerEnemyling(this.sprite.body);

        this.splatBitmap = splatBitmap;

        this.sprite.body.onBeginContact.add((other) => {
            var kind = <SpriteKind>other.sprite["kind"];
            if (kind === SpriteKind.Platform) {
                this.onGround = true;
            } else if (kind === SpriteKind.DynamoSplat) {
                this.splat();
            }
        }, this);
    }

    splat() {
        var ghost = new InklingGhost(this.game);
        ghost.create(this.sprite.position.x, this.sprite.position.y, true); 

        this.dead = true;
        this.sprite.visible = false;
        this.sprite.destroy();
        this.state.enemylingDied(this);
    }

    update() {
        if (this.dead) {
            this.respawnTimer -= this.game.time.physicsElapsedMS;
        } else if (this.sprite.position.x <= 100) {
            this.sprite.body.velocity.x = 0;
            this.state.playerDied();
        } else if (this.onGround) {
            this.sprite.body.velocity.x = -100;
            this.lastFireTime += this.game.time.physicsElapsedMS;

            if (this.lastFireTime >= 200) {
                this.lastFireTime = 0;
                var splat = this.game.add.sprite(this.sprite.position.x - 35, this.sprite.position.y - 5, this.splatBitmap);
                splat["kind"] = SpriteKind.EnemySplat;
                this.game.physics.p2.enableBody(splat, false);
                splat.scale.set(3, 3);
                splat.anchor.set(0.5, 0.5);
                splat.smoothed = false;
                splat.body.velocity.x = -600;
                splat.body.velocity.y = -70;
                this.collisions.registerEnemyling(splat.body);
            }
        }
    }
}
