﻿class KillNotifications {
    game: Phaser.Game;
    groups: Phaser.Group[];

    constructor(game: Phaser.Game) {
        this.game = game;
        this.groups = [];
    }

    add(name: string) {
        this.shiftUp();

        var group = this.game.add.group();
        this.groups.push(group);
        group.position.setTo(this.game.world.centerX + 300, this.game.world.height - 20);
        group.alpha = 0;

        var box = this.game.add.sprite(0, -2, "notificationbox", null, group);
        box.scale.setTo(3, 3);
        box.smoothed = false;
        box.anchor.setTo(0.5, 0.5);

        var text = this.game.add.text(0, 0, "Splatted " + name, { fontSize: 16, fill: "#000000" }, group);
        text.anchor.setTo(0.5, 0.5);

        this.animateIn(group);
    }

    shiftUp() {
        var expectedY = this.game.world.height - 20;
        this.groups.forEach(g => {
            expectedY -= 25;
            if (g.position.y > expectedY) {
                var tween = this.game.add.tween(g);
                tween.to({ y: expectedY }, 200, Phaser.Easing.Linear.None, true);
            }
        });
    }

    animateIn(group: Phaser.Group) {
        var tween1 = this.game.add.tween(group);
        var tween2 = this.game.add.tween(group);
        tween1.to({ x: this.game.world.centerX, alpha: 1 }, 200, Phaser.Easing.Linear.None);

        tween2.to({ alpha: 0 }, 300, Phaser.Easing.Linear.None);
        tween2.onComplete.add(() => {
            var idx = this.groups.indexOf(group);
            if (idx >= 0) {
                this.groups.splice(idx, 1);
            }
            group.destroy();
        });
        tween2.delay(2000);

        tween1.chain(tween2);
        tween1.start();
    }
}