﻿class MainState extends Phaser.State {
    map: GameMap;
    collisions: CollisionHelper;
    dynamo: DynamoRoller;
    enemylingGroup: Phaser.Group;
    tutorialTween: Phaser.Tween;
    killNotifications: KillNotifications;

    enemylings: Enemyling[];
    enemylingSkins: number[];
    enemylingNames: string[];
    enemylingSplat: Phaser.BitmapData;
    enemylingInitialSpawnTimer = 1000;

    kills = 0;

    constructor(collisions: CollisionHelper) {
        super();
        this.collisions = collisions;
    }

    preload() {
        this.game.load.spritesheet("inkling", "content/inkling.png", 32, 40);
        this.game.load.spritesheet("inksplats", "content/inksplats.png", 7, 7);
        this.game.load.spritesheet("enemylings", "content/enemylings.png", 24, 29);
        this.game.load.spritesheet("ghost", "content/ghost.png", 10, 10);
        this.game.load.image("notificationbox", "content/notificationbox.png");
    }

    create() {
        this.kills = 0;
        this.enemylingNames = [];
        this.enemylingSkins = [];
        while (this.enemylingNames.length < 4) {
            var name = this.game.rnd.pick(DynamoSimulator.playerNames);
            if (this.enemylingNames.indexOf(name) < 0) {
                this.enemylingNames.push(name);
            }
            this.enemylingSkins.push(this.game.rnd.integerInRange(0, 3));
        }
        
        this.dynamo = new DynamoRoller(this);
        this.map = new GameMap(this.game);
        this.killNotifications = new KillNotifications(this.game);

        this.game.stage.backgroundColor = "#ffffff";
        
        // Physics
        this.collisions.create();

        // Actors
        this.map.create(this.collisions);
        this.dynamo.create(this.collisions);

        var tutorial = this.game.add.text(25, 130, "Press space (or touch) to flick your roller", { fontSize: 16, fill: "#000000" });
        this.tutorialTween = this.game.add.tween(tutorial).to({ alpha: 0 });

        this.enemylingGroup = this.game.add.physicsGroup(Phaser.Physics.P2JS);
        this.enemylings = [];
        this.enemylingSplat = this.game.make.bitmapData(3, 3);
        this.enemylingSplat.circle(1, 1, 2, "#00ff00");
    }

    update() {
        if (isActionButtonDown(this.game) && this.tutorialTween) {
            this.tutorialTween.start();
            this.tutorialTween = null;
        }

        this.dynamo.update();

        this.enemylings.forEach((e, i) => {
            e.update();
            if (e.respawnTimer < 0) {
                e = new Enemyling(this, e.name, e.baseFrame);
                e.create(this.collisions, this.enemylingGroup, this.enemylingSplat);
                this.enemylings[i] = e;
            }
        });

        if (this.enemylings.length < 4) {
            this.enemylingInitialSpawnTimer += this.game.time.physicsElapsedMS;
            if (this.enemylingInitialSpawnTimer > 2000) {
                var enemy = new Enemyling(this, this.enemylingNames[this.enemylings.length], this.enemylingSkins[this.enemylings.length]);
                enemy.create(this.collisions, this.enemylingGroup, this.enemylingSplat);
                this.enemylings.push(enemy);
                this.enemylingInitialSpawnTimer = 0;
            }
        }
    }

    shutdown() {
        this.map.destroy();
    }

    enemylingDied(e: Enemyling) {
        this.kills++;
        this.killNotifications.add(e.name);
    }

    playerDied() {
        this.dynamo.kill();
    }

    switchToDead() {
        this.game.state.start("dead", true, false, this.kills);
    }
}
