﻿class CollisionHelper {
    game: Phaser.Game;
    platformGroup: Phaser.Physics.P2.CollisionGroup;
    dynamoSplatGroup: Phaser.Physics.P2.CollisionGroup;
    enemySplatGroup: Phaser.Physics.P2.CollisionGroup;
    enemylingGroup: Phaser.Physics.P2.CollisionGroup;

    constructor(game: Phaser.Game) {
        this.game = game;
    }
    
    create() {
        this.platformGroup = this.game.physics.p2.createCollisionGroup();
        this.dynamoSplatGroup = this.game.physics.p2.createCollisionGroup();
        this.enemySplatGroup = this.game.physics.p2.createCollisionGroup();
        this.enemylingGroup = this.game.physics.p2.createCollisionGroup();
    }

    registerPlatform(platform: Phaser.Physics.P2.Body) {
        platform.setCollisionGroup(this.platformGroup);
        platform.collides([this.dynamoSplatGroup, this.enemySplatGroup, this.enemylingGroup]);
    }

    registerDynamoSplat(splat: Phaser.Physics.P2.Body) {
        splat.setCollisionGroup(this.dynamoSplatGroup);
        splat.collides([this.platformGroup, this.enemylingGroup]);
    }

    registerEnemyling(enemyling: Phaser.Physics.P2.Body) {
        enemyling.setCollisionGroup(this.enemylingGroup);
        enemyling.collides([this.platformGroup, this.dynamoSplatGroup]);
    }
}