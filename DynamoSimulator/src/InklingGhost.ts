﻿class InklingGhost {
    game: Phaser.Game;
    sprite: Phaser.Sprite;
    onComplete: Phaser.Signal;

    constructor(game: Phaser.Game) {
        this.game = game;
    }

    create(x: number, y: number, isEnemy: boolean) {
        this.sprite = this.game.add.sprite(x, y, "ghost", isEnemy ? 1 : 0);
        this.sprite.scale.set(3, 3);
        this.sprite.anchor.set(0.5, 0.5);
        this.sprite.smoothed = false;

        var tween1 = this.game.add.tween(this.sprite).to({ x: x + 20, y: y - 40 }, 500, Phaser.Easing.Quadratic.InOut);
        var tween2 = this.game.add.tween(this.sprite).to({ x: x, y: y - 90 }, 500, Phaser.Easing.Quadratic.InOut);
        var tween3 = this.game.add.tween(this.sprite).to({ x: x + 20, y: y - 130, alpha: 0 }, 500, Phaser.Easing.Quadratic.InOut);

        tween1.chain(tween2, tween3);
        tween3.onComplete.add(() => { this.sprite.destroy(); });
        this.onComplete = tween3.onComplete;

        tween1.start();
    }
}