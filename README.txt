This is a Splatoon fangame.
To build & run it you need to:

1. Go into the "DynamoSimulator" directory (where the subfolders "content" and "src" are located)
2. Compile the TypeScript sources into a JavaScript file
3. Start a web server (any will do, e.g. "python -m http.server 4141")
4. Open your browser and go to the URL on which your server is listening.

If you have Visual Studio (at least VS2013 Update 2) you only need to open the "DynamoSimulator.sln"
and press F5. This will do all these things.

If you don't have Visual Studio, first get the TypeScript compiler from http://www.typescriptlang.org/.
Then call "tsc --sourcemap --out app.js deps/*.d.ts src/*" (on linux) or start "build.cmd" (on windows).
