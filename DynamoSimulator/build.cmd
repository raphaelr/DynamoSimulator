echo deps/p2.d.ts > typescript-filelist.txt
echo deps/phaser.comments.d.ts >> typescript-filelist.txt
echo deps/pixi.comments.d.ts >> typescript-filelist.txt

dir /b /s src >> typescript-filelist.txt
tsc --sourcemap --out app.js @typescript-filelist.txt
del typescript-filelist.txt
