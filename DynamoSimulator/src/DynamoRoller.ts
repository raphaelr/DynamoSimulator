﻿class DynamoRoller {
    game: Phaser.Game;
    map: GameMap;
    state: MainState;

    dynamo: Phaser.Sprite;
    splatGroup: Phaser.Group;
    collisions: CollisionHelper;

    flickDuration = 0.0;
    flickState = 0.0;
    dead = false;

    constructor(state: MainState) {
        this.game = state.game;
        this.state = state;
    }

    create(collisions: CollisionHelper) {
        this.splatGroup = this.game.add.physicsGroup(Phaser.Physics.P2JS);
        this.collisions = collisions;

        // dynamo roller, set anchor to fat part of the handle
        this.dynamo = this.game.add.sprite(20, 152, "inkling");
        this.dynamo.scale.setTo(3, 3);
        this.dynamo.smoothed = false;

        // flick animation
        this.dynamo.animations.add("flick", [
            // Wind up
            0, 0, 0, 1, 1, 1, 2, 2, 2,
            // Flick
            2, 1, 0
        ], 12 / 1, false);
    }

    update() {
        if (this.dead) {
            return;
        }

        var isActionDown = isActionButtonDown(this.game);
        if (this.flickState === 0 && isActionDown) {
            this.flickState = 1;
            this.flickDuration = 0;
            this.dynamo.play("flick");
            this.game.add.tween(this.dynamo);
        }
        if (this.flickState === 1 && this.flickDuration >= 750) {
            this.flickState = 2;
            this.doFlickDamage();
        }
        if (this.flickState === 2 && this.flickDuration >= 1000 && !isActionDown) {
            this.flickState = 0;
        }
        if (this.flickState !== 0) {
            this.flickDuration += this.game.time.physicsElapsedMS;
        }
    }

    doFlickDamage() {
        var count = normalRandom(this.game, 30, 10);
        if (count < 25) count = 25;

        for (var i = 0; i < count; i++) {
            var frame = this.game.rnd.between(0, 2);
            var x = normalRandom(this.game, 60, 10);
            var y = normalRandom(this.game, 170, 10);
            var rotation = this.game.rnd.realInRange(0, 2 * Math.PI);
            var vx = this.game.rnd.realInRange(120, 1200);
            var vy = 0;

            var splat = this.game.add.sprite(x, y, "inksplats", frame);
            splat["kind"] = SpriteKind.DynamoSplat;
            splat.scale.set(3, 3);
            splat.anchor.set(0.5, 0.5);
            splat.rotation = rotation;
            splat.smoothed = false;
            this.splatGroup.add(splat);
            splat.body.velocity.x = vx;
            splat.body.velocity.y = vy;
            this.collisions.registerDynamoSplat(splat.body);
        }
    }

    kill() {
        if (this.dead) {
            return;
        }

        var ghost = new InklingGhost(this.game);
        ghost.create(this.dynamo.position.x, this.dynamo.position.y, false);
        ghost.onComplete.add(() => { this.state.switchToDead(); });

        this.dead = true;
        this.dynamo.destroy();
    }
}
