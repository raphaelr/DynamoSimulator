﻿class GameMap {
    game: Phaser.Game;
    collisions: CollisionHelper;
    platformGroup: Phaser.Group;
    overlaySprite: Phaser.Sprite;
    overlayData: Phaser.BitmapData;
    tile: Phaser.BitmapData;

    constructor(game: Phaser.Game) {
        this.game = game;
    }


    create(collisions: CollisionHelper) {
        this.collisions = collisions;
        this.tile = this.game.make.bitmapData(1, 1);
        this.tile.fill(0x60, 0x60, 0x60);

        var t2 = this.game.make.bitmapData(1, 1);
        t2.fill(0xff, 0, 0);

        this.platformGroup = this.game.add.physicsGroup(Phaser.Physics.P2JS);
        this.addRect(0, 262, 63, 218);
        this.addRect(50, 348, 800, 132);
        this.addRect(36, 301, 200, 80, 0.44168369999999996);

        this.overlayData = this.game.make.bitmapData(850, 480);
        this.overlaySprite = this.game.add.sprite(0, 0, this.overlayData);
    }

    addRect(xMin: number, yMin: number, w: number, h: number, rotation = 0) {
        var sprite = this.platformGroup.create(xMin + w / 2, yMin + h / 2, this.tile);
        sprite["kind"] = SpriteKind.Platform;
        sprite.scale.setTo(w, h);

        sprite.body.setRectangle(w, h);
        sprite.body.static = true;
        sprite.body.rotation = rotation;

        this.collisions.registerPlatform(sprite.body);

        sprite.body.onBeginContact.add((other) => {
            var kind = <SpriteKind>other.sprite["kind"];
            if (kind === SpriteKind.EnemySplat || kind === SpriteKind.DynamoSplat) {
                this.collisionCallback(other, sprite.body, kind);
            }
        });
    }

    collisionCallback(splat: Phaser.Physics.P2.Body, platform: Phaser.Physics.P2.Body, splatKind: SpriteKind) {
        // Calculate top left corner of impact platform
        var minPoint = new Phaser.Point();
        this.getGlobalPoint(minPoint, platform.sprite, 0, 0);

        // Caclulate the line we're colliding with
        var k = Math.tan(platform.rotation);
        var d = minPoint.y - k * minPoint.x;

        // ... and extract the segment that touches the splat
        var x1 = splat.sprite.position.x - splat.sprite.anchor.x * splat.sprite.width;
        var x2 = splat.sprite.position.x + splat.sprite.anchor.x * splat.sprite.width;

        // Ensure that we don't overextend
        this.getGlobalPoint(minPoint, platform.sprite, 1, 0);
        if (x2 > minPoint.x) {
            x2 = minPoint.x;
        }

        var y1 = k * x1 + d;
        var y2 = k * x2 + d;

        var color = splatKind === SpriteKind.DynamoSplat ? "#0026FF" : "#00FF00";
        var lineWidth = 10;
        this.overlayData.line(x1, y1 + lineWidth / 2, x2, y2 + lineWidth / 2, color, lineWidth);
        this.overlayData.dirty = true;

        // Kill the splat
        splat.removeNextStep = true;
        splat.sprite.pendingDestroy = true;
    }

    getGlobalPoint(p: Phaser.Point, sprite: Phaser.Sprite, localX: number, localY: number) {
        p.setTo(localX, localY);
        p.subtract(sprite.anchor.x, sprite.anchor.y);
        p.multiply(sprite.width, sprite.height);
        p.rotate(0, 0, sprite.rotation);
        p.add(sprite.position.x, sprite.position.y);
    }

    destroy() {
        this.overlayData.destroy();
    }
}