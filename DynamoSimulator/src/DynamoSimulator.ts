﻿class DynamoSimulator {
    game: Phaser.Game;
    collisions: CollisionHelper;

    constructor() {
        this.game = new Phaser.Game(850, 480, Phaser.AUTO, "content", {
            create: this.create.bind(this),
        });
        this.collisions = new CollisionHelper(this.game);
    }

    create() {
        this.game.scale.scaleMode = Phaser.ScaleManager.SHOW_ALL;
        this.game.scale.maxWidth = 850;
        this.game.scale.maxHeight = 480;

        this.game.stage.backgroundColor = "#ffffff";
        this.game.physics.startSystem(Phaser.Physics.P2JS);
        this.game.physics.p2.gravity.y = 700;
        this.game.physics.p2.restitution = 0;

        this.collisions.create();

        this.game.state.add("dead", DeadState);
        this.game.state.add("main", () => new MainState(this.collisions), true);

        console.log("DynamoSimulator ⓒ 2016 Raphael Robatsch");
        console.log("A Splatoon fangame - http://dynamosimulator.tapesoftware.net");
    }

    static playerNames = ["Tim", "TAKA", "Christian", "", ". . . . .",
        "Splatoon", "48/49", "GreenCallie", "Milton", "Some meme",
        "Tape", "♪Fuchsia♪", "matt_νøξ", "かずお(Kazuo)", "Kev'nシ", "™Justice", "Iggly"];
}

enum SpriteKind {
    Platform,
    DynamoSplat,
    EnemySplat
}

window.onload = () => {
    var game = new DynamoSimulator();
    window["$gameObject"] = game;
};
